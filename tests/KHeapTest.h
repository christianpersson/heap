//
// Created by Christian Persson on 18/03/15.
//

#ifndef _ARRAYHEAP_KHEAPTEST_H_
#define _ARRAYHEAP_KHEAPTEST_H_

#include <ctime>
#include "../ArrayHeap/KHeap.h"
#include <cmath>
#include <map>

void kHeapTest() {

    int N = 1000000;
    int R = 10*N;

    std::cout << "Test KHeap\n";
    KHeap<> k;


    std::cout << "\nInsert " << N << " elements in random order\n";
    auto t = clock();
    for (int i = 0; i < N; i++) {
        k.set(i, rand() % R);
    }
    t = clock() - t;
    k.print();
    std::cout << " >> Done in " << t / 1000 << "ms" << std::endl;


    std::cout << "\nRemove all elements \n";
    t = clock();
    for (int i = 0; i < N; i++) {
        k.erase(i);
    }
    k.print();
    std::cout << " >> Done in " << (clock() - t) / 1000 << "ms" << std::endl;


    std::cout << "\nInsert " << N << " elements in random order\n";
    t = clock();
    // Test pop
    for (int i = 0; i < N; i++) {
        k.set(i, rand() % R);
    }
    std::cout << " >> Done in " << (clock() - t) / 1000 << "ms" << std::endl;


    std::cout << "\nCompare with map\n";
    t = clock();
    std::map<int, int> map;
    for (int i = 0; i < N; i++) {
        map[i] = rand() % R;
    }
    std::cout << " >> Done in " << (clock() - t) / 1000 << "ms" << std::endl;

    t = clock();
    for (int i = 0; i < N; i++) {
        map.erase(i);
    }
    std::cout << " >> Done in " << (clock() - t) / 1000 << "ms" << std::endl;


    std::cout << "\nPop all elements \n";
    t = clock();
    for (int i = 0; i < N; i++) {
        k.pop();
    }
    k.print();
    std::cout << " >> Done in " << (clock() - t) / 1000 << "ms" << std::endl;


    std::cout << "\nPop and print 10 elements \n";
    for (int i = 0; i < 10; i++) {
        k.set(i, rand() % R);
    }
    while(!k.empty()){
        std::cout << k.get() << std::endl;
        k.pop();
    }


//    k.print();



}

#endif //_ARRAYHEAP_KHEAPTEST_H_
