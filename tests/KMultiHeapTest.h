//
// Created by Christian Persson on 18/03/15.
//

#ifndef _HEJ_KMULTIHEAPTEST_H_
#define _HEJ_KMULTIHEAPTEST_H_

#include "../ArrayHeap/KMultiHeap.h"
#include <iostream>
#include <ctime>
#include <cmath>


void kMultiHeapTest1() {

    auto t = clock();
    KMultiHeap h;
    int N = 5000;
    int N2 = N * N;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j <= i; j++) {
            h.set(i, j, rand() % N2);
        }
    }
    std::cout << "Added " << N << " elements in " << (clock() - t) / 1000 << " ms\n";
    t = clock();

    for (int i = 0; i < N; i++) {
        h.remove(i);
    }
    std::cout << "Remove " << N << " elements in " << (clock() - t) / 1000 << " ms\n";

//    h.print();
    h.remove(3);
//    h.print();

}

void kMultiHeapTest2() {
    auto t = clock();
    int N = 200;
    int k = 5;
    KMultiHeap h;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j <= i; j++) {
            h.set(i, j, i);
        }
    }
    std::cout << "Added " << N << " elements in " << (clock() - t) / 1000 << " ms\n";
    t = clock();

    for (int i = 0; i < N - k; i++) {
        h.remove(i);
    }

    std::cout << "Removed " << N - k << " elements in " << (clock() - t) / 1000 << " ms\n";

    h.print();
}

void kMultiHeapTest() {
    kMultiHeapTest1();
//    kMultiHeapTest2();
}

#endif //_HEJ_kMULTIHEAPTEST_H_
