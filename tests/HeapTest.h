//
// Created by Christian Persson on 17/03/15.
//

#ifndef _HEJ_HEAPTEST_H_
#define _HEJ_HEAPTEST_H_

#include "../ArrayHeap/Heap.h"
#include <iostream>


void heapTest1() {
    std::cout << "Starting heaptest.... \n";
    Heap<> h;
    h.set(12);
    h.set(1);

    h.set(32);
    h.set(11);
    h.set(431);
    h.set(-1);

    std::cout << h.top() << std::endl;
    h.pop();

    while (h.size() > 0) {
        std::cout << h << std::endl;
        h.pop();
    }
}

void heapTest2() {
    Heap<> h;
    for (int i = 0; i < 1000; i++) {
        h.set(i);
    }
    for (int i = 0; i < 980; i++) {
        h.pop();
    }
    while (h.size() > 0) {
        std::cout << h.top() << std::endl;
        h.pop();
    }
}

void heapTest() {
    heapTest1();
    heapTest2();
}

#endif //_HEJ_HEAPTEST_H_
