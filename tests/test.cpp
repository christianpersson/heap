////#include "ArrayHeapTest.h"
////#include "HeapTest.h"
//#include "KHeapTest.h"
//#include "KMultiHeapTest.h"

#include <iostream>
#include <cmath>
#include <ctime>
#include <limits>
#include <vector>
#include "../lib.h"

#include "../DDist.h"
struct H {
    int p_in, p_out;

    inline static double getP(const H &_) {
        return _.p_in * _.p_out;
    }
};


int main() {


    DDist d1, d2;
    int N = 1000000;


    for(int i = 0 ; i < N; i++){
        d1.set(2*i, rand());
    }

    for(int i = 0 ; i < N; i++){
        d2.set(2*i+1, rand());
    }

    std::cout << d1 << std::endl;
    std::cout << d2 << std::endl;

    auto t = clock();
    d1 += d2;
    std::cout << (clock() - t)/1000 << " ms\n";

    std::cout << d1 << std::endl;



    return 0;
}
