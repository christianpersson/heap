//
// Created by Christian Persson on 17/03/15.
//

#ifndef _HEJ_ARRAYHEAPTEST_H_
#define _HEJ_ARRAYHEAPTEST_H_

#include "../ArrayHeap/ArrayHeap.h"
#include <cmath>
#include <iostream>


void arrayHeapTest1() {
    ArrayHeap<> h;

    h.set(3, 23);
    h.set(5, 32);
    h.set(3, 32);

    std::cout << h.top() << std::endl;

    h.set(3, 76);

    std::cout << h.top() << std::endl;

    h.get(3) = 65;
    h.update(3);

    std::cout << std::endl;
    for (auto &node : h) {
        std::cout << node << std::endl;
    }
}

void arrayHeapTest2() {
    std::cout << "Test\n";
    ArrayHeap<> h;

    for (int i = 0; i < 100; i++) {
        h.set(i, rand());
    }

    for (int i = 0; i < 90; i++) {
        h.remove(i);
    }


    std::cout << std::endl;
    std::cout << h.top() << std::endl;
    for (auto &node : h) {
        std::cout << node << std::endl;
    }
}

void test_if_it_can_add_a_prev_removed_value(){
    ArrayHeap<> h;
    h.set(1,23);
    std::cout << "Size: " << h.size() << std::endl;
    h.pop();
    std::cout << "Size: " << h.size() << std::endl;

    h.set(1,32);
    std::cout << "Size: " << h.size() << std::endl;


}

void arrayHeapTest() {
//    arrayHeapTest1();
    arrayHeapTest2();
    test_if_it_can_add_a_prev_removed_value();
}

#endif //_HEJ_ARRAYHEAPTEST_H_
