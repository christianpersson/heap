//
// Created by Christian Persson on 21/03/15.
//

#ifndef _ARRAYHEAP_DDIST_H_
#define _ARRAYHEAP_DDIST_H_

#include <unordered_map>


class DDist {
    typedef std::unordered_map<int, double> Container;
    typedef std::pair<int, double> CPair;

    Container m_dist;
    double sum = 0;
    double FlogF = 0;

public:

    double getSum() const {
        return sum;
    }

    double entropy() const {
        return log2(sum) - FlogF / sum;
    }

    int size() const {
        return m_dist.size();
    }

    bool empty() const {
        return m_dist.empty();
    }

    friend std::ostream &operator<<(std::ostream &os, const DDist &obj) {
        return os << obj.getSum() << " " << obj.entropy();
    }

    const typename Container::const_iterator begin() const {
        return m_dist.begin();
    }

    const typename Container::const_iterator end() const {
        return m_dist.end();
    }

    void set(const int key, const double value) {
        set(CPair(key, value));
    }

    inline typename Container::iterator set(const CPair p) {
        return set(p, m_dist.begin());
    }

    inline typename Container::iterator set(const CPair p, Container::iterator hint) {
        auto it = m_dist.insert(hint, p);
        if (it->second != p.second) {
            _remove(it->second);
            it->second = p.second;
        }
        _insert(p.second);
        return it;
    }

    void erase(const int key) {
        auto it = m_dist.find(key);
        if (it != m_dist.end()) {
            _remove(it->second);
            m_dist.erase(it);
        }
    }

    Container::iterator find(const int key) {
        return m_dist.find(key);
    }

    DDist &operator+=(const DDist &right) {
        for (auto &e : right.m_dist) {
            set(e);
        }
        return *this;
    }

private:
    inline void _insert(const double value) {
        FlogF += value * log2(value > 0 ? value : -value);
        sum += value;
    }

    inline void _remove(const double value) {
       _insert(-value);
    }
};


#endif //_ARRAYHEAP_DDIST_H_
